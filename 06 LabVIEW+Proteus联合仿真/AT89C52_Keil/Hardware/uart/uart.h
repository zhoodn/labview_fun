#ifndef __UART_H
#define __UART_H

#include <reg52.h>

extern unsigned char Rec_flag;
extern unsigned char Rec_data;

void uart_init(void);
void SendByte(unsigned char d);

#endif