#include "uart.h"

unsigned char Rec_flag = 0;
unsigned char Rec_data = 0;

void uart_init()
{
    TMOD |= 0x20;//定时器1，工作模式2 8位自动重装
    TH1 = 0xfd;
    TL1 = 0xfd;//设置比特率9600
    SM0 = 0;
    SM1 = 1;//串口工作方式1,8位UART波特率可变
    TR1 = 1;//启动定时器1 
    REN = 1;
    EA = 1;	//打开总中断
    ES = 1; //打开串口中断  
}

void SendByte(unsigned char dat) //发送一个字节的数据，形参d即为待发送数据。
{
    SBUF = dat; //将数据写入到串口缓冲
    while(!TI); //等待发送完毕
}

void uart(void) interrupt 4		 //串口中断
{
    unsigned char Res;
    static unsigned char Rec_state = 0;

    if(RI)    //收到数据
    {
        RI = 0;   //清中断请求
        Res = SBUF;
        
        if(Res == 0x11) Rec_state = 1;
        else if(Rec_state == 1 && Res == 0x22) Rec_state = 2; 
        else if(Rec_state == 2 && Res == 0x33) Rec_state = 3;
        else if(Rec_state == 3 && Res == 0x44) 
        {
            Rec_state = 0;
            Rec_flag = 1;
        }
        else
        {
            Rec_state = 0;
        }
    }
    else        //发送完一字节数据
    {
        TI = 0;
    }
}

