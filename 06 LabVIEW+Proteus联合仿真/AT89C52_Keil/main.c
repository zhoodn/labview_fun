#include <reg52.h>
#include "uart.h"
#include <stdlib.h>

void SendData(void);

void delay_ms(int xms)
{
    unsigned char i;
    for( ; xms > 0; xms--)
        for(i = 110; i > 0; i--);
}

float Data[2] = {0};

float wave[8] = {13.5,6.4,10.8,15.3,16.3,18,15,12};

void main()
{    
    unsigned char i = 0;   
    
    uart_init(); 
          
    while(1)
    {
        if(Rec_flag)//是否收到1帧数据
        {
            delay_ms(500);
            
            Data[0] = rand()%12;
            Data[1] = rand()%12;
            
            i++;
            SendData(); 
            
            Rec_flag = 0;
        }
    }
}

void SendData(void)
{ 
    int i;
    unsigned char *p = (unsigned char *)& (*Data);   //发送数据
    
    SendByte(0xAA);
    SendByte(0xBB);
    SendByte(0xCC);
    SendByte(0xDD);
    
    for(i = 8; i > 0; i--)
    {
       SendByte(*(p + 8 - i));
    }
    
    SendByte(0xDD);
    SendByte(0xCC);
    SendByte(0xBB);
    SendByte(0xAA);
}

