## 项目介绍
 **项目1：** 用LabVIEW做一个查询天气的小工具。

演示视频：[LabVIEW && 天气预报_哔哩哔哩_bilibili](https://www.bilibili.com/video/bv1Tp4y1b7mF)



 **项目2：** 用LabVIEW编写的飞机大战小游戏。

演示视频：[用 LabVIEW 写一个『✈️飞机大战』小游戏_哔哩哔哩_bilibili](https://www.bilibili.com/video/BV1Cq4y1E74U)

 

**项目3：** 用LabVIEW获取B站粉丝数。

演示视频（带教程）：[今 天 涨 粉 了 吗_哔哩哔哩_bilibili](https://www.bilibili.com/video/BV1Ly4y137KM)



**项目4：** LabVIEW BadApple。

演示视频：[LabVIEW 也 能 放 BadApple ？_哔哩哔哩_bilibili](https://www.bilibili.com/video/BV1cX4y1T7bw)



 **项目5：** LabVIEW +Proteus 遥控小车

演示视频：[用LabVIEW来操作遥控小车 「Proteus+LabVIEW联合仿真」](https://www.bilibili.com/video/BV1sh411C7X7)



**项目6：** LabVIEW +Proteus联合仿真

演示视频：[Proteus和LabVIEW能碰撞出什么样的火花？ 实现串口数据传输及波形显示_哔哩哔哩_bilibili](https://www.bilibili.com/video/BV1fe411s7wX/)



**项目7：** LabVIEW 贪吃蛇

演示视频：[LabVIEW 贪吃蛇](https://www.bilibili.com/video/BV12d4y1f7Vz/)
